import pygame,sys
from draw import draw
def choose_map(WIN,MINIMAPS,BASE_FONT,clock,FPS,game_instance):
    INPUT_RECT1,INPUT_RECT2,INPUT_RECT3 = pygame.Rect(150, 150, 250, 32),pygame.Rect(550, 150, 250, 32),pygame.Rect(400, 300, 100, 32)
    USER_NAME1,USER_NAME2,LAPS = '', '',''
    color_active = pygame.Color('lightskyblue3')
    color_passive = pygame.Color('grey') 
    color1,color2,color3 = color_passive,color_passive,color_passive
    active1,active2,active3 = False, False,False
    text1,text2,text3,text4 = BASE_FONT.render("Wybierz mapę",1,"black"),BASE_FONT.render("Nazwa gracza: 1",1,"black"),BASE_FONT.render("Nazwa gracza: 2",1,"black"),BASE_FONT.render("Wpisz ilość okrążeń",1,"black")
    WIN.fill("white")
    run = True
    while run:
        WIN.fill((255, 255, 255)) 
        WIN.blit(text1,(350,400))
        WIN.blit(text2,(150,120))
        WIN.blit(text3,(550,120))
        WIN.blit(text4,(100,305))
        draw(WIN,MINIMAPS)
        clock.tick(FPS)
        
        for event in pygame.event.get(): 
            if event.type == pygame.QUIT: 
                pygame.quit() 
                run=False
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN: 
                if INPUT_RECT1.collidepoint(event.pos): 
                    active1,active2,active3 = True,False,False
                else: 
                    active1 = False
                if INPUT_RECT2.collidepoint(event.pos): 
                    active2,active1,active3 = True,False,False
                else: 
                    active2 = False
                if INPUT_RECT3.collidepoint(event.pos): 
                    active3,active1,active2 = True,False,False
                else: 
                    active3 = False
                pos = pygame.mouse.get_pos()
                if pos[0] > 100 and pos[0] < 400 and pos[1] > 500 and pos[1] < 800:
                    if USER_NAME1 != "" and len(LAPS) > 0:
                        game_instance.map_id=1
                        run=False
                        break
                if pos[0] > 500 and pos[0] < 800 and pos[1] > 500 and pos[1] < 800:
                    if USER_NAME1 != "" and len(LAPS) > 0:
                        run=False
                        game_instance.map_id=2
                        break
            if event.type == pygame.KEYDOWN:
                if active1:
                    if event.key == pygame.K_BACKSPACE: 
                        USER_NAME1 = USER_NAME1[:-1] 
                    else: 
                        if len(USER_NAME1) < 8:
                            USER_NAME1 += event.unicode
                if active2:
                    if event.key == pygame.K_BACKSPACE: 
                        USER_NAME2 = USER_NAME2[:-1] 
                    else:
                        if len(USER_NAME2) < 8:
                            USER_NAME2 += event.unicode
                if active3:
                    if event.key == pygame.K_BACKSPACE: 
                        LAPS = LAPS[:-1] 
                    else:
                        if len(LAPS) > 0:
                            if event.unicode in ["1","2","3","4","5","6","7","8","9","0"] and len(LAPS) < 3:
                                LAPS += event.unicode
                        else:
                            if event.unicode in ["1","2","3","4","5","6","7","8","9"] and len(LAPS) < 3:
                                LAPS += event.unicode   
                if event.key == pygame.K_ESCAPE:
                    return False
        if active1: 
            color1 = color_active
        else: 
            color1 = color_passive 
        if active2: 
            color2 = color_active
        else: 
            color2 = color_passive 
        if active3:
            color3 = color_active
        else:
            color3 = color_passive
        
        pygame.draw.rect(WIN, color1, INPUT_RECT1) 
        pygame.draw.rect(WIN, color2, INPUT_RECT2) 
        pygame.draw.rect(WIN, color3, INPUT_RECT3)
        text_surface1 = BASE_FONT.render(USER_NAME1, True, (255, 255, 255)) 
        text_surface2 = BASE_FONT.render(USER_NAME2, True, (255, 255, 255)) 
        text_surface3 = BASE_FONT.render(LAPS, True, (255, 255, 255)) 
        INPUT_RECT1.w = max(100, text_surface1.get_width()+10)
        INPUT_RECT2.w = max(100,text_surface2.get_width()+10)
        INPUT_RECT3.w = max(100,text_surface3.get_width()+10)
        WIN.blit(text_surface1, (INPUT_RECT1.x+5, INPUT_RECT1.y+5))
        WIN.blit(text_surface2, (INPUT_RECT2.x+5, INPUT_RECT2.y+5))
        WIN.blit(text_surface3, (INPUT_RECT3.x+5, INPUT_RECT3.y+5))
        pygame.display.update()
    game_instance.player1_name, game_instance.player2_name, game_instance.laps_to_go =USER_NAME1,USER_NAME2, int(LAPS)
    return True