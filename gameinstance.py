import pygame, json
class GameInstance:

    def __init__(self) -> None:
        self.player1_name=''
        self.player2_name=''
        self.map_id=1
        self.laps_to_go=2
        self.winner=''
        self.fp= "score.json"
    def initialize_level(self,MAP_DATA):
        self.TRACK:pygame.surface.Surface = pygame.image.load(f"assets/map{self.map_id}/map_track.png")
        self.TRACK_BORDER:pygame.surface.Surface = pygame.image.load(f"assets/map{self.map_id}/map_border.png")
        self.TRACK_BORDER_MASK:pygame.mask.Mask = pygame.mask.from_surface(self.TRACK_BORDER)
        self.GRASS:pygame.surface.Surface = pygame.image.load(f"assets/map{self.map_id}/map_bg.png")
        self.FINISH:pygame.surface.Surface = pygame.image.load("assets/finish/finish.png")
        self.FINISH_MASK:pygame.mask.Mask = pygame.mask.from_surface(self.FINISH)
        self.FINISH_POSITION = MAP_DATA[self.map_id]["finish_position"]
        self.player1_start= MAP_DATA[self.map_id]["player1_start"]
        self.player2_start = MAP_DATA[self.map_id]["player2_start"]
        self.player_rotation = MAP_DATA[self.map_id]["player_rotation"]
        self.images=[(self.GRASS,(0,0)),(self.TRACK,(0,0)),(self.FINISH,self.FINISH_POSITION),(self.TRACK_BORDER,(0,0))]

        for player_name in [self.player1_name, self.player2_name]:
            if player_name != "":
                self.prepare_data(player_name)

    def prepare_data(self,player_name):
        with open(self.fp,"r") as file:
            data= json.load(file)
        z=-1
        for player in data:
            if player_name == player:
                z=1
                break
        if z == -1:
            with open(self.fp,"w") as file:
                data.update({player_name:{ 1:{"best_time":0,"wins":0},2:{"best_time":0,"wins":0}}})
                json.dump(data,file)

    def save_score(self,player_name,best_time_of_race):
        with open(self.fp,"r") as file:
            data= json.load(file)
        z=-1
        for player in data:
            if player_name == player:
                z=1
                if player_name == self.winner:
                    z=2
                break
        if z != -1:
            with open(self.fp,"w") as file:
                print(data[player_name][str(self.map_id)]["best_time"],best_time_of_race)
                if data[player_name][str(self.map_id)]["best_time"] > best_time_of_race or data[player_name][str(self.map_id)]["best_time"] == 0:
                    data[player_name][str(self.map_id)]["best_time"] = best_time_of_race
                
                if z == 2:
                    data[player_name][str(self.map_id)]["wins"] += 1
    
                json.dump(data,file)
