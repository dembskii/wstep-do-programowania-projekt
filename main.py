from static import * 
from mainmenu import main_menu
pygame.init()
global game_instance
global clock
BASE_FONT = pygame.font.Font(None, 32)
WIN_FONT = pygame.font.Font(None, 64)
game_instance = GameInstance()

def game():
    game_instance.initialize_level(MAP_DATA=MAP_DATA)
    player1 = CarBase(game_instance.player1_name,game_instance.player_rotation,game_instance.player1_start,CAR_1)
    players=[player1]
    if game_instance.player2_name != '':
        player2 = CarBase(game_instance.player2_name,game_instance.player_rotation,game_instance.player2_start,CAR_2)
        players.append(player2)
    pygame.time.set_timer(pygame.USEREVENT,10)

    run=True
    while run:
        clock.tick(FPS)
        player1_time=BASE_FONT.render(f"Current time:{convert_ms_to_s(player1.current_time)}",1,"white")
        player1_laps=BASE_FONT.render(f"Laps: {player1.laps}/{game_instance.laps_to_go}",1,"white")
        
        
        draw(WIN,game_instance.images,players)
        WIN.blit(player1_time,(10,10))
        WIN.blit(player1_laps,(10,40))
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run=False
                sys.exit()
                break

            if event.type == pygame.USEREVENT:
                for player in players:
                    player.current_time += 10
        
        handle_movement_player(player1,game_instance.TRACK_BORDER_MASK,game_instance.FINISH_MASK,game_instance.FINISH_POSITION)
        if len(players) == 2:
            handle_movement_player2(player2,game_instance.TRACK_BORDER_MASK,game_instance.FINISH_MASK,game_instance.FINISH_POSITION)
            player2_time=BASE_FONT.render(f"Current time:{convert_ms_to_s(player2.current_time)}",1,"white")
            player2_laps=BASE_FONT.render(f"Laps: {player2.laps}/{game_instance.laps_to_go}",1,"white")
            WIN.blit(player2_time,(680,10))
            WIN.blit(player2_laps,(800,40))
        for player in players:
            if player.laps == game_instance.laps_to_go:
                game_instance.winner = player.name
                WIN.blit(source=WIN_FONT.render(f"{game_instance.winner} wins!", True, (255, 255, 255)),dest=(350,400))
                pygame.display.update()
                pygame.time.delay(2000)
                run=False
                break
        pygame.display.update()
    for player in players:
        game_instance.save_score(player.name,player.best_time)

run=True 
clock = pygame.time.Clock()

while run:
    choice = main_menu(WIN,MAIN_MENU_IMAGES,clock,FPS)
    if choice == "play":
        play = choose_map(WIN,MINIMAPS,BASE_FONT,clock,FPS,game_instance)
        if play == True:
            game()
        else:
            continue
    elif choice == "leaderboard":
        leaderboard(WIN,clock,FPS)

    

pygame.quit()
