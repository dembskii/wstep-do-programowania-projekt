
def draw(win, images,players=None):
    for image, pos in images:
        win.blit(image, pos)
    if players!=None:
        for player in players:
            player.draw(win)