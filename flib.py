import pygame

def scale_image(image,k):
    size = round(image.get_width() * k), round(image.get_height() * k)
    return pygame.transform.scale(image,size)

def blit_rotate_center(win, image, top_left, angle):
    rotated_image = pygame.transform.rotate(image,angle)
    new_rect = rotated_image.get_rect(center=image.get_rect(topleft = top_left).center)
    win.blit(rotated_image,new_rect.topleft)

def convert_ms_to_s(ms):
    return f"{ms//1000},{ms%1000}s"