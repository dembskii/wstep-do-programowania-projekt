import pygame,json
from flib import convert_ms_to_s

def leaderboard(WIN,clock,FPS):
    BASE_FONT = pygame.font.Font(None, 30)
    TITLE_FONT = pygame.font.Font(None, 50)
    with open("score.json","r") as file:
        data=json.load(file)
        prepared_data=[[],[]]
        for i in [1,2]:
            for player in data:
                    player_data=data[player][str(i)]
                    prepared_data[i-1].append((player,player_data["best_time"],player_data["wins"]))
            prepared_data[i-1] = bubble_sort(prepared_data[i-1])        
    run=True
    
    while run:
        WIN.fill("white")
        clock.tick(FPS)
        RECT1=pygame.Rect(600, 800, 250, 32)
        RECT2=pygame.Rect(100, 800, 250, 32)
        title = TITLE_FONT.render("Leaderboard", 1, (0, 0, 0))


        map1_text = BASE_FONT.render("Top 5 - Map 1", 1, (0, 0, 0))
        map2_text = BASE_FONT.render("Top 5 - Map 2", 1, (0, 0, 0))
        pygame.draw.rect(WIN, pygame.Color('lightskyblue3'), RECT1) 
        pygame.draw.rect(WIN, pygame.Color('lightskyblue3'), RECT2) 
        by_wins = BASE_FONT.render("sort by wins", True, (0, 0, 0)) 
        by_time = BASE_FONT.render("sort by best time", True, (0, 0, 0)) 
        WIN.blit(by_wins, (RECT1.x+5, RECT1.y+5))
        WIN.blit(by_time, (RECT2.x+5, RECT2.y+5))

        WIN.blit(title,(350,100))
        WIN.blit(map1_text,(100,200))
        WIN.blit(map2_text,(600,200))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run=False
                break

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    run=False
                    break
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                if pos[0] > 100 and pos[0] < 350 and pos[1] > 800 and pos[1] < 832:
                    for i in [1,2]:
                        prepared_data[i-1] = bubble_sort(prepared_data[i-1],"best_time")
                if pos[0] > 600 and pos[0] < 850  and pos[1] > 800 and pos[1] < 832:
                    for i in [1,2]:
                        prepared_data[i-1] = bubble_sort(prepared_data[i-1])
        for i in [0,1]:
            offset=0
            for name, best_time, wins in prepared_data[i][:5]:
                
                text_surface1 = BASE_FONT.render(name, 1, (0, 0, 0))
                text_surface2 = BASE_FONT.render(convert_ms_to_s(best_time), 1, (0, 0, 0))
                text_surface3 = BASE_FONT.render(str(wins), 1, (0, 0, 0))
        
                if i == 0:
                    WIN.blit(text_surface1, (50, 300+offset))
                    WIN.blit(text_surface2, (170, 300+offset))
                    WIN.blit(text_surface3, (290, 300+offset))

                if i == 1:
                    WIN.blit(text_surface1, (550, 300+offset))
                    WIN.blit(text_surface2, (670, 300+offset))
                    WIN.blit(text_surface3, (790, 300+offset))
                offset += 100

        pygame.display.update()
        
def bubble_sort(data:list,sort_by="wins"):
    for i in range(len(data)-1):
         for j in range(len(data)-i-1):
                if sort_by == "wins":
                   if data[j][2] < data[j+1][2]:
                        data[j],data[j+1] = data[j+1],data[j] 
                elif sort_by == "best_time":
                    if data[j][1] > data[j+1][1]:
                        data[j],data[j+1] = data[j+1],data[j]

    if sort_by == "best_time":
        index = 0
        for i in range(len(data)-1):
            if data[i][1] == 0:
                index = i
        if index != 0:
                
            items_to_remove=data[:index+1]
            for elem in items_to_remove:
                data.remove(elem)
            for elem in items_to_remove:
                data.append(elem)
    return data