#IMPORT
import time
import sys
import math
import pygame
import requests
from leaderboard import *
from flib import *
from carbase import *
from carhandling import handle_movement_player, handle_movement_player2
from gameinstance import *
from choosemap import choose_map
from draw import *
#STATIC
FPS=60
MINIATURE_1:pygame.surface.Surface = scale_image(pygame.image.load("assets/miniatures/miniature1.png"),0.3)
MINIATURE_2:pygame.surface.Surface = scale_image(pygame.image.load("assets/miniatures/miniature2.png"),0.3)
LOGO:pygame.surface.Surface = scale_image(pygame.image.load("assets/logo/f1_logo.jpg"),0.2)
START_GAME_BTN:pygame.surface.Surface = scale_image(pygame.image.load("assets/buttons/btn_startgame.png"),1)
QUIT_GAME_BTN:pygame.surface.Surface = scale_image(pygame.image.load("assets/buttons/btn_quitgame.png"),1)
LEADERBORD_BTN:pygame.surface.Surface = scale_image(pygame.image.load("assets/buttons/btn_leaderboard.png"),1)
CAR_1:pygame.surface.Surface = scale_image(pygame.image.load("assets/cars/car1.png"),0.05)
CAR_2:pygame.surface.Surface = scale_image(pygame.image.load("assets/cars/car3.png"),0.05)
MAIN_MENU_IMAGES=[(START_GAME_BTN,(300,350)),(QUIT_GAME_BTN,(300,550)),(LOGO,(300,100)),(LEADERBORD_BTN,(300,450))]
MINIMAPS=[(MINIATURE_1,(100,500)),(MINIATURE_2,(500,500))]
#Window
WIDTH, HEIGHT = 900, 900
WIN = pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption("Formula 1 Project") 

MAP_DATA={
    1: {
        "player1_start" : (120,350),
        "player2_start" : (180,350),
        "finish_position" : (110,400),
        "player_rotation": 0
    },
    2: {
        "player1_start" : (70,350),
        "player2_start" : (110,350),
        "finish_position" : (40,400),
        "player_rotation" : 0
    }
}
