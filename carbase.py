from flib import *
import math

class CarBase:
    def __init__(self,name,initial_rotation,initial_position,image) -> None:
        self.name = name
        self.image = image
        self.initial_position=initial_position
        self.max_velocity = 5
        self.velocity = 0
        self.rotation_velocity = 4
        self.angle = initial_rotation
        self.x, self.y = initial_position
        self.acceleration = 0.1
        self.laps=0
        self.best_time=0
        self.current_time=0


    def rotate(self,left=False,right=False):
        if left:
            self.angle += self.rotation_velocity
        elif right:
            self.angle -= self.rotation_velocity

    def draw(self,win):
        blit_rotate_center(win, self.image,(self.x,self.y),self.angle)

    def accelerate(self):
        self.velocity = min(self.velocity + self.acceleration,self.max_velocity)
        self.move()

    def deaccelerate(self):
        self.velocity = max(self.velocity - self.acceleration, -self.max_velocity/2)
        self.move()

    def idle(self):
        if self.velocity < 0:
            self.velocity = min(self.velocity + self.acceleration/2 , 0)
        else:
            self.velocity = max(self.velocity - self.acceleration/2, 0)
        self.move()

    def move(self):
        radians = math.radians(self.angle)
        vertical = math.cos(radians) * self.velocity
        horizontal = math.sin(radians) * self.velocity

        self.x -= horizontal
        self.y -= vertical

    def collide(self,mask,x=0,y=0):
        car_mask = pygame.mask.from_surface(self.image)
        offset= (int(self.x - x), int(self.y - y))
        poi = mask.overlap(car_mask, offset)
        return poi
    
    def bounce(self):
        self.velocity = -self.velocity/2
        self.move()

    def reset(self):
        self.y = self.initial_position[1]
        self.laps += 1
        self.compare_time()

    def compare_time(self):
        if self.current_time < self.best_time or self.best_time == 0:
            self.best_time = self.current_time
        
        self.current_time = 0

        