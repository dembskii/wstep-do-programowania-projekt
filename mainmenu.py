import pygame
from draw import draw
import sys
def main_menu(WIN,IMAGES,clock,FPS):
    WIN.fill("white")
    run=True
    while run:
        clock.tick(FPS)
        draw(win=WIN,images=IMAGES)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()

                if pos[0] > 300 and pos[0] < 572 and pos[1] < 418 and pos[1] > 350:
                    run=False
                    return "play"
                if pos[0] > 300 and pos[0] < 572 and pos[1] < 518 and pos[1] > 450:
                    run=False
                    return "leaderboard"

                if pos[0] > 300 and pos[0] < 572 and pos[1] < 618 and pos[1] > 550:
                    run=False
                    pygame.quit()
                    sys.exit()
