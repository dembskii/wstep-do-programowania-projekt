import pygame
import pytest
from flib import convert_ms_to_s, scale_image
from leaderboard import bubble_sort
from gameinstance import GameInstance
from carbase import CarBase
import json


def test_convert_ms_to_s():
    assert convert_ms_to_s(1395) == "1,395s"

def test_scale_image():
    IMAGE:pygame.surface.Surface=pygame.image.load("assets/miniatures/miniature1.png")
    k=0.2
    image_scaled = scale_image(IMAGE, k)
    assert (IMAGE.get_width(),IMAGE.get_height()) == (image_scaled.get_width()/k,image_scaled.get_height()/k)


def test_bubble_sort():
    data=[("Dominik",11111,4),("Maks",1132111,10),("Mateusz",10001,2)]
    assert bubble_sort(data,"wins") == [("Maks",1132111,10),("Dominik",11111,4),("Mateusz",10001,2)]


def test_prepare_data():
    fp = "score_test.json"
    game_instance = GameInstance()
    game_instance.fp = fp
    game_instance.prepare_data("Dominik")

    with open(fp,"r") as file:
        data = json.load(file)
    
    assert data == {"Dominik":{ "1":{"best_time":0,"wins":0},"2":{"best_time":0,"wins":0}}}

def test_compare_items():
    IMAGE:pygame.surface.Surface=pygame.image.load("assets/miniatures/miniature1.png")
    car=CarBase("Dominik",0,(0,0),IMAGE)
    time_to_check=12313
    car.current_time = time_to_check
    car.best_time = 20480
    car.compare_time()

    assert car.best_time == time_to_check

