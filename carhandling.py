import pygame


def handle_movement_player(player,TRACK_BORDER_MASK,FINISH_MASK,FINISH_POSITION):
    keys = pygame.key.get_pressed()
    pressed_w = False
    if keys[pygame.K_a]:
        player.rotate(left=True)
    if keys[pygame.K_d]:
        player.rotate(right=True)
    if keys[pygame.K_w]:
        pressed_w = True
        player.accelerate()
    if keys[pygame.K_s]:
        pressed_w = True
        player.deaccelerate()
    if not pressed_w:
        player.idle()

    if player.collide(TRACK_BORDER_MASK) != None:
        player.bounce()

    finish_poi_collide=player.collide(FINISH_MASK,*FINISH_POSITION)
    if finish_poi_collide != None:
        if finish_poi_collide[1] == 0:
            player.bounce()
        else:
            player.reset()


def handle_movement_player2(player,TRACK_BORDER_MASK,FINISH_MASK,FINISH_POSITION):
    keys = pygame.key.get_pressed()
    pressed_w = False
    if keys[pygame.K_LEFT]:
        player.rotate(left=True)
    if keys[pygame.K_RIGHT]:
        player.rotate(right=True)
    if keys[pygame.K_UP]:
        pressed_w = True
        player.accelerate()
    if keys[pygame.K_DOWN]:
        pressed_w = True
        player.deaccelerate()
    if not pressed_w:
        player.idle()

    if player.collide(TRACK_BORDER_MASK) != None:
        player.bounce()

    finish_poi_collide=player.collide(FINISH_MASK,*FINISH_POSITION)
    if finish_poi_collide != None:
        if finish_poi_collide[1] == 0:
            player.bounce()
        else:
            player.reset()